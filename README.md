Directory Structure:
--------------------

Source: This folder contains the source file main.py

Results: This folder contains the output images

Usage: [Question Number] [Input_Options] [Output Options]
[Question Number]
1 Histogram equalization
2 Frequency domain filtering
3 Laplacian pyramid blending
[Input Options]
Path to the input images
[Output Options]
Output directory
Example usages:
Source/main.py 1 [path to input image] [output directory]
Source/main.py 2 [path to input image1] [path to input image2] [output directory]
Source/main.py 3 [path to input image1] [path to input image2] [output directory]
